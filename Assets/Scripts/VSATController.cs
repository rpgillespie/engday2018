﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSATController : MonoBehaviour {
    public float speed = 1F;
    public Font textfont;

    private Quaternion from, from2;
    private Quaternion to, to2;
    private float ztime;

    private List<float> az_list;
    private List<float> el_list;

    public void addAzEl(float az, float el)
    {
        az_list.Add(az);
        el_list.Add(el);
    }

    public void nextAzEl()
    {
        float azimuth = az_list[0];
        float elevation = el_list[0];
        az_list.RemoveAt(0);
        el_list.RemoveAt(0);

        // Debug.LogFormat("AE: {0} {1}", azimuth, elevation);
        
        ztime = 0;
        from = transform.rotation;
        from2 = transform.GetChild(1).localRotation;
        to = Quaternion.Euler(0, azimuth, 0);
        to2 = Quaternion.Euler(90.0F - elevation, 0, 0);
    }

    // OnGUI
    void OnGUI()
    {
        int w = 200, h = 20;
        Rect rectObj = new Rect((Screen.width - w) / 2, Screen.height - 3*h, w, h);
        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.LowerCenter;
        style.richText = true;
        style.font = textfont;
        style.fontSize = 40;
        GUI.Box(rectObj, 
            "Azimuth: <color=#1b5574ff>" + transform.rotation.eulerAngles.y.ToString("F1") + "°</color>\t" +
            "Elevation: <color=#20a548ff>" + (90.0F - transform.GetChild(1).localRotation.eulerAngles.x).ToString("F1") + "°</color>", style);
    }

    void Start()
    {
        az_list = new List<float>();
        el_list = new List<float>();
        addAzEl(0,0);
        nextAzEl();
    }

    void Update()
    {
        if (ztime * speed > 1 && az_list.Count > 0)
            nextAzEl();

        ztime += Time.deltaTime;
        transform.rotation = Quaternion.Lerp(from, to, ztime * speed);
        transform.GetChild(1).localRotation = Quaternion.Lerp(from2, to2, ztime * speed);

        if (Input.GetKeyDown("r"))
        {
            addAzEl(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 90.0f));
        }
    }
}
