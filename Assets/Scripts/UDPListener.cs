﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using SimpleJSON;

public class UDPListener : MonoBehaviour
{
    public int port = 8051;
    public GameObject go;
    
    Thread receiveThread;
    private bool kill = false;
    private bool show_gui = false;

    UdpClient client;
    private bool new_message = false;
    private string lastReceivedUDPPacket = "";
    private float az = 0.0f;
    private float el = 0.0f;

    // start from unity3d
    public void Start()
    {
        Debug.Log("Test with: nc -u 127.0.0.1  " + port + "");

        Application.runInBackground = true;
        receiveThread = new Thread(new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();
    }

    // OnGUI
    void OnGUI()
    {
        if (show_gui) {
            Rect rectObj = new Rect(40, 10, 200, 400);
            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = 20;
            GUI.Box(rectObj, 
                "# UDP Listener\n" + 
                "# 127.0.0.1:" + port, style);
        }
    }

    void Update()
    {
        if (new_message)
        {
            string msg = lastReceivedUDPPacket;
            new_message = false;
            var json = JSON.Parse(msg);

            foreach (var coordinate in json["body"]["coordinates"])
            {
                az = coordinate.Value["azimuth_degree"].AsFloat;
                el = coordinate.Value["elevation_degree"].AsFloat;
                go.GetComponent<VSATController>().addAzEl(az, el);
            }
        }

        if (Input.GetKeyDown("space"))
            show_gui = !show_gui;
    }

    void OnDestroy()
    {
        Debug.Log("Killing listener thread...");
        kill = true;
        IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
        byte[] data = Encoding.UTF8.GetBytes("kill");
        client.Send(data, data.Length, remoteEndPoint);
    }

    // receive thread
    private void ReceiveData()
    {
        client = new UdpClient(port);

        while (!kill)
        {
            try
            {
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = client.Receive(ref anyIP);
                string text = Encoding.UTF8.GetString(data);
                Debug.Log("Received: " + text);
                lastReceivedUDPPacket = text;
                new_message = true;
            }
            catch (Exception err)
            {
                Debug.Log(err.ToString());
            }
        }

        Debug.Log("Listener dead.");
    }
}